import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View } from "react-native";
import React, { useState, useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { NativeBaseProvider } from "native-base";
import { LogBox } from 'react-native';
LogBox.ignoreAllLogs();

import Home from "./src/view/home/Home";
import Caderno from "./src/view/caderno/Caderno";
import Camera from "./src/view/camera/Camera";

const stack = createStackNavigator();

import theme from "./src/model/Theme";

export default function App() {
  return (
    <NativeBaseProvider theme={theme}>
      <NavigationContainer>
        <stack.Navigator
          initialRouteName="Login"
          screenOptions={{
            headerShown: false,
          }}
        >
          <stack.Screen name="Home" component={Home} />
          <stack.Screen name="Caderno" component={Caderno} />
          <stack.Screen name="Camera" component={Camera} />
        </stack.Navigator>
        <StatusBar style="auto" />
      </NavigationContainer>
    </NativeBaseProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
