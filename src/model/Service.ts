import axios from "axios";

export const GetDados = async () => {
  var config = {
    method: "get",
    maxBodyLength: Infinity,
    url: "http://192.168.1.140:3000/dados",
    headers: {
    },
  };
  let resposta;
  await axios(config)
    .then(function (response) {
      resposta = response.data;
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });
  return resposta;
};

export const AddCaderno = async (caderno) => {
  let resposta;
  var data = JSON.stringify({
    caderno: caderno,
  });

  var config = {
    method: "post",
    maxBodyLength: Infinity,
    url: "http://192.168.1.140:3000/addCaderno",
    headers: {
      "Content-Type": "application/json",
    },
    data: data,
  };

  await axios(config)
    .then(function (response) {
      resposta = response.data;
    })
    .catch(function (error) {
      console.log(error);
    });
  return resposta;
};
