import React from "react";
import { extendTheme } from "native-base";
const theme = extendTheme({
  colors: {
    // Add new color
    amarelo: {
      50: "#fff4cd",
      100: "#fdeaa7",
      200: "#f8df85",
      300: "#f1d264",
      400: "#edc841",
      500: "#e4bd31",
      600: "#d9b225",
      700: "#be9e28",
      800: "#a48a2a",
      900: "#8c772b",
    },

    marrom: {
      50: "#ff9553",
      100: "#ff7c2b",
      200: "#ff6403",
      300: "#da5400",
      400: "#b34400",
      500: "#963c05",
      600: "#7c3408",
      700: "#622c0a",
      800: "#4a230a",
      900: "#341909",
    },

    // Redefining only one shade, rest of the color will remain same.
    amber: {
      400: "#d97706",
    },
  },
  config: {
    // Changing initialColorMode to 'dark'
    initialColorMode: "dark",
  },
});
export default theme;
