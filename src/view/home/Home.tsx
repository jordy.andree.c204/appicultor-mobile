import React from 'react'
import {View, Text} from 'react-native'
import Categorias from '../components/categorias/Categorias'
import Header from '../components/header/Header'
import styles from './Home_Styles'

const Home = (props) =>{
    return(
        <View style={styles.container}>
            <Header nav={props.navigation} page={"Home"} paginaAtual={'Home'}/>
            <Categorias nav={props.navigation}/>
        </View>
    )
}

export default Home