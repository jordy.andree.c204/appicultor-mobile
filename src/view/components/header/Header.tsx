import React from "react";
import { Image, IconButton, View, Text } from "native-base";
import { AntDesign } from "@expo/vector-icons";

import styles from "./Header_Styles";

const logo = require("../../../assets/logo-coofamel.png");
const Header = (props) => {
  let atual_page = props.paginaAtual

  const HandlePage =(atual_page) =>{
    if(atual_page == "Home"){
      return(
        <></>
      )
    }else{
      return(
        <IconButton
        position={"absolute"}
        left={5}
        colorScheme={"amarelo"}
        size={"md"}
        variant="solid"
        onPress={() => props.nav.navigate(props.page)}
        _icon={{
          as: AntDesign,
          name: "arrowleft",
        }}
      />
      )
    }
  }
  return (
    <View style={styles.container}>
      <Image mt={5} source={logo} alt={"logo"} />
      <View
        position={'absolute'}
        flexDir={"row"}
        w={"100%"}
        alignItems={"center"}
        justifyContent={"center"}
        bottom={5}
      >
        {HandlePage(atual_page)}
        <View bg={'white'} p={2} borderRadius={10} w={'40%'} alignItems={'center'} m={-2}>
          <Text color={'black'}>{atual_page}</Text>
        </View>
      </View>
    </View>
  );
};

export default Header;
