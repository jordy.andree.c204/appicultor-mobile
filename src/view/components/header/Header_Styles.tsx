import { position } from "native-base/lib/typescript/theme/styled-system";
import { StyleSheet, Dimensions } from "react-native";

const styles = StyleSheet.create({
    container:{
        flex:0.29,
        width: Dimensions.get('window').width,
        alignItems: 'center',
        backgroundColor: '#b34400',
        marginBottom: 20,
    }
})

export default styles