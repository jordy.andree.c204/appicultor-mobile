import React from "react";
import { Image, IconButton, View, Text, Pressable} from "native-base";
import { AntDesign } from "@expo/vector-icons";

import styles from "./Categorias_Style";

const caderno = require("../../../assets/escrita.png");

const Categorias = (props) => {
  return (
    <View style={styles.container}>
      <View bg={"black"} w="50%" ml={5} p={3} borderRadius={10}>
        <Text fontSize={16} fontWeight={"bold"} color={"amarelo.400"}>
          Produtor
        </Text>
      </View>
      <View w={'100%'} flexDir={"row"}>
        <View
          borderRadius={15}
          alignItems={"center"}
          display={"flex"}
          justifyContent={"center"}
          p={3}
          ml={5}
          mt={5}
          bg={"black"}
          w={"25%"}
        >
          <Image w={65} h={65} source={caderno} alt={"caderno"} />
          <Text fontSize={16} fontWeight={"bold"} color={"amarelo.400"}>
            Cadastrar Colmeias
          </Text>
        </View>
        <View
          borderRadius={15}
          alignItems={"center"}
          display={"flex"}
          justifyContent={"center"}
          p={3}
          ml={5}
          mt={5}
          bg={"black"}
          w={"25%"}
        >
          <Image w={65} h={65} source={caderno} alt={"caderno"} />
          <Text fontSize={16} fontWeight={"bold"} color={"amarelo.400"}>
            Visita Técnica
          </Text>
        </View>
        <Pressable w={'100%'} onPress={()=>{
            props.nav.navigate('Caderno')
        }}>
          <View
            borderRadius={15}
            alignItems={"center"}
            display={"flex"}
            justifyContent={"center"}
            p={3}
            ml={5}
            mt={5}
            bg={"black"}
            w={"25%"}
          >
            <Image w={65} h={65} source={caderno} alt={"caderno"} />
            <Text fontSize={16} fontWeight={"bold"} color={"amarelo.400"}>
              Caderno Eventos
            </Text>
          </View>
        </Pressable>
      </View>
    </View>
  );
};

export default Categorias;
