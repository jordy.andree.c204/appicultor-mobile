import React from "react";
import {
  View,
  Text,
  Input,
  IconButton,
  FlatList,
  Fab,
  Icon,
  Modal,
  Button,
  CheckIcon,
  Select,
  Checkbox,
  TextArea,
} from "native-base";
import Header from "../components/header/Header";
import styles from "./Caderno_Styles";
import { AntDesign } from "@expo/vector-icons";
import { AddCaderno, GetDados } from "../../model/Service";
import { Colmeia } from "../../model/Interfaces";
import { compareSpecificity } from "native-base/lib/typescript/hooks/useThemeProps/propsFlattener";
import { Keyboard, TouchableWithoutFeedback } from "react-native";

const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback
    onPress={() => {
      Keyboard.dismiss();
    }}
    accessible={false}
  >
    {}
    {children}
  </TouchableWithoutFeedback>
);

const Caderno = (props) => {
  const [req, setReq] = React.useState<any>([]);
  const [dados, setDados] = React.useState<any>();
  const [nomeProdutor, setNomeProdutor] = React.useState();
  const [nomeApiario, setNomeApiario] = React.useState();
  const [cadernos_array, setCadernos] = React.useState<any>();
  const [isOpen, setIsOpen] = React.useState(false);
  const [service, setService] = React.useState<any>({
    Descrição: "",
    Opções: [],
  });
  const [opcao, setOpcao] = React.useState<any>();
  const [colmeia, setColmeia] = React.useState<any>();
  const [apiarios, setApiario] = React.useState<any>();
  const [obs, setObs] = React.useState<any>();
  const [quantidade, setQuantidade] = React.useState(0);
  const [scaneado, setScaneado] = React.useState("");
  const [again, setAgain] = React.useState(0);
  const date = new Date();

  let dataHoje = `${
    date.getDate() < 10 ? "0" + date.getDate() : date.getDate()
  }/${
    date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1
  }/${date.getFullYear()}`;

  React.useEffect(() => {
    GetDados()
      .then((r) => {
        console.log(r)
        FilterComeia(
          r.Colmeias,
          r.Produtores,
          r.ProdutoresXApiarios,
          r.Caderno,
          r.Eventos
        );
      })
      .catch((err) => console.log(err));
  }, [again]);

  const FilterComeia = (colmeias, produtores, apiarios, cadernos, eventos) => {
    let colmeiaEscolhida;
    colmeias.forEach((elem) => {
      if (elem != undefined) {
        if (elem.QRCode == scaneado) {
          colmeiaEscolhida = elem;
          setColmeia(elem);
          setDados(colmeiaEscolhida.Colmeia);
        }
      }
    });
    setReq(eventos);
    FilterProdutor(produtores, colmeiaEscolhida);
    FilterApiario(apiarios, colmeiaEscolhida);
    FilterCadernos(cadernos, colmeiaEscolhida);
  };
  const FilterProdutor = (produtores, colmeiaInfo) => {
    if (produtores != undefined)
      produtores.forEach((elem) => {
        if (colmeiaInfo.Cadastro != undefined) {
          if (elem.Cadastro == colmeiaInfo.Cadastro) {
            setNomeProdutor(elem.Nome);
          }
        }
      });
  };
  const FilterApiario = (apiarios, colmeiaInfo) => {
    if (apiarios != undefined)
      apiarios.forEach((elem) => {
        if (colmeiaInfo.Apiario != undefined) {
          if (elem.seqApiario == colmeiaInfo.Apiario) {
            console.log(elem.Nome);
            setNomeApiario(elem.Nome);
            setApiario(elem.seqApiario);
          }
        }
      });
  };
  const FilterCadernos = (cadernos, colmeiaInfo) => {
    let cadernos_array = [];
    if (cadernos != undefined) {
      cadernos.forEach((elem) => {
        if (elem.SeqColmeia == colmeiaInfo.seqColmeia) {
          cadernos_array.push(elem);
        }
      });
    }
    console.log(colmeiaInfo);
    setCadernos(cadernos_array);
  };
  const HandleModal = () => {
    setIsOpen(true);
    setService({ Descrição: "Escolha um Evento", Opções: [] });
  };
  const HandleOptions = () => {
    if (service.Opções != undefined) {
      return service.Opções.map((elem, i) => {
        if (elem == "Quantidade") {
          return (
            <View flexDir={"row"} alignItems={"center"} w={"100%"}>
              <Text ml={5} fontSize={22} fontWeight={"bold"} color={"black"}>
                {elem}
              </Text>
              <Input
                keyboardType="numeric"
                color={"black"}
                ml={3}
                fontSize={22}
                size={"xs"}
                placeholder="QTD."
                onChangeText={(elem) => setQuantidade(Number(elem))}
                w="20%"
              />
            </View>
          );
        } else {
          return (
            <View flexDir={"row"} w={"100%"}>
              <Checkbox.Group onChange={(elem) => setOpcao(elem)}>
                <Checkbox
                  colorScheme={"amarelo"}
                  ml={5}
                  value={elem}
                  size={"lg"}
                  p={5}
                  accessibilityLabel="This is a dummy checkbox"
                />
              </Checkbox.Group>
              <Text ml={3} fontSize={25} fontWeight={"bold"} color={"black"}>
                {elem}
              </Text>
            </View>
          );
        }
      });
    }
  };
  console.log(service);
  console.log(opcao);
  return (
    <DismissKeyboard>
      <View style={styles.container}>
        <Header nav={props.navigation} page="Home" paginaAtual={"Caderno"} />
        <View flex={1} alignItems={"center"} w={"100%"}>
          <Input
            w={"80%"}
            size="xl"
            placeholder="Código Colmeia"
            InputRightElement={
              <IconButton
                onPress={() => {
                  props.navigation.navigate("Camera", {
                    setScaneado: setScaneado,
                    nav: props.navigation,
                    setAgain: setAgain,
                  });
                }}
                colorScheme={"amarelo"}
                size={"lg"}
                variant="solid"
                _icon={{
                  as: AntDesign,
                  name: "search1",
                }}
              />
            }
          />
          <View
            pb={2}
            borderBottomColor={"black"}
            borderBottomWidth={1}
            mt={5}
            w={"100%"}
            flexDir={"row"}
            justifyContent={"space-evenly"}
          >
            <View w={"33%"} alignItems={"center"}>
              <Text fontSize={16} fontWeight={"bold"} color={"black"}>
                Produtor
              </Text>
              <Text color={"black"} fontSize={16}>
                {nomeProdutor}
              </Text>
            </View>
            <View w={"33%"} alignItems={"center"}>
              <Text fontSize={16} fontWeight={"bold"} color={"black"}>
                Apiario
              </Text>
              <Text color={"black"} fontSize={16}>
                {nomeApiario}
              </Text>
            </View>
            <View w={"33%"} alignItems={"center"}>
              <Text fontSize={16} fontWeight={"bold"} color={"black"}>
                Colmeia
              </Text>
              <Text color={"black"} fontSize={16}>
                {dados}
              </Text>
            </View>
          </View>
          <FlatList
            extraData={cadernos_array}
            w={"100%"}
            getItemLayout={(data, index) => ({
              length: 100,
              offset: 100 * index,
              index,
            })}
            data={cadernos_array}
            renderItem={(item: any) => (
              <TouchableWithoutFeedback>
                <View m={2} borderRadius={12} p={3} bg={"amarelo.400"}>
                  <Text mb={2} color={"black"} fontSize={16}>
                    <Text fontSize={16} fontWeight={"bold"} color={"black"}>
                      Data:{" "}
                    </Text>
                    {item.item.datEvento}
                  </Text>
                  <Text fontSize={16} fontWeight={"bold"} color={"black"}>
                    Evento Notificado:
                  </Text>
                  <Text color={"black"} mb={2} fontSize={16}>
                    {item.item.obsEvento}
                  </Text>
                  <Text color={"black"} fontSize={16}>
                    <Text fontSize={16} fontWeight={"bold"} color={"black"}>
                      Quantidade:{" "}
                    </Text>
                    {item.item.quantidade}
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            )}
          />
          <Fab
            onPress={() => {
              HandleModal();
            }}
            colorScheme={"amarelo"}
            bg={"black"}
            renderInPortal={false}
            shadow={2}
            size="sm"
            icon={
              <Icon color="amarelo.400" as={AntDesign} name="plus" size="xl" />
            }
          />
        </View>

        <Modal isOpen={isOpen}>
          <DismissKeyboard>
            <Modal.Content w={"100%"}>
              <View bg={"white"} alignItems={"center"} w="100%" h="100%">
                <View w={"100%"}>
                  <View
                    bg={"#b34400"}
                    alignItems={"center"}
                    h={"18%"}
                    justifyContent={"center"}
                    w={"100%"}
                  >
                    <Text fontSize={22} fontWeight={"bold"} color={"black"}>
                      Registro de Evento
                    </Text>
                  </View>
                  <Text
                    mt={3}
                    ml={5}
                    fontSize={20}
                    fontWeight={"bold"}
                    color={"black"}
                  >
                    Data:{" "}
                    <Text
                      ml={5}
                      fontSize={20}
                      fontWeight={"bold"}
                      color={"black"}
                    >
                      {dataHoje}
                    </Text>
                  </Text>
                  <Text
                    mt={3}
                    ml={5}
                    fontSize={20}
                    fontWeight={"bold"}
                    color={"black"}
                  >
                    Evento:
                  </Text>
                  <Select
                    fontSize={16}
                    fontWeight={"bold"}
                    color={"black"}
                    ml={5}
                    w="82%"
                    accessibilityLabel="Choose Service"
                    placeholder={service.Descrição}
                    _selectedItem={{
                      bg: "teal.600",
                      endIcon: <CheckIcon size={5} />,
                    }}
                    mt="1"
                    onOpen={() =>
                      setService({
                        Descrição: "",
                        Opções: [],
                      })
                    }
                    onValueChange={(itemValue) => {
                      setService(itemValue);
                    }}
                  >
                    {req.map((elem) => {
                      return (
                        <Select.Item label={elem.Descrição} value={elem} />
                      );
                    })}
                  </Select>
                  <Text
                    mt={3}
                    ml={5}
                    mb={3}
                    fontSize={20}
                    fontWeight={"bold"}
                    color={"black"}
                  >
                    Opções:
                  </Text>
                  {HandleOptions()}
                  <Text
                    mt={2}
                    ml={5}
                    fontSize={20}
                    fontWeight={"bold"}
                    color={"black"}
                  >
                    Observações:
                  </Text>
                  <TextArea
                    fontSize={16}
                    fontWeight={"bold"}
                    color={"black"}
                    ml={5}
                    mb={5}
                    h={24}
                    placeholder="Adicone uma Observação"
                    w="80%"
                    maxW="300"
                    autoCompleteType={undefined}
                    onChangeText={setObs}
                  />
                </View>
                <View
                  bottom={5}
                  position={"absolute"}
                  w={"100%"}
                  flexDir={"row"}
                  justifyContent={"space-evenly"}
                >
                  <Button
                    w={"40%"}
                    colorScheme={"amarelo"}
                    onPress={() => {
                      let dataSend = {
                        seqApiario: apiarios,
                        SeqColmeia: colmeia.seqColmeia,
                        seqEvento: service.seqEvento,
                        datEvento: dataHoje,
                        obsEvento: obs,
                        quantidade: quantidade,
                      };
                      AddCaderno(dataSend)
                        .then((r) => {
                          FilterComeia(
                            r.Colmeias,
                            r.Produtores,
                            r.ProdutoresXApiarios,
                            r.Caderno,
                            r.Eventos
                          );
                          setIsOpen(false);
                        })
                        .catch((err) => console.log(err));
                      console.log(dataSend);
                    }}
                  >
                    <Text color={"black"}>Registrar</Text>
                  </Button>
                  <Button
                    w={"40%"}
                    bg={"black"}
                    colorScheme={"amarelo"}
                    onPress={() => setIsOpen(false)}
                  >
                    <Text color={"amarelo.400"}>Cancelar</Text>
                  </Button>
                </View>
              </View>
            </Modal.Content>
          </DismissKeyboard>
        </Modal>
      </View>
    </DismissKeyboard>
  );
};

export default Caderno;
