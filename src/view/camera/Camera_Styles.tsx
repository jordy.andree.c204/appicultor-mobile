import {StyleSheet, Dimensions} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: '#b34400',
  },

  header: {
    width: '100%',
    height: 70,
    backgroundColor: '#b34400',
    justifyContent: 'center',
    borderBottomColor: 'white',
    borderBottomWidth: 5,
    alignItems: 'center',
  },

  headerText: {
    fontWeight: 'bold',
    color: '#FFF',
    fontSize: 18,
  },

  containerButtons: {
    width: '100%',
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    position: 'absolute',
    bottom: 15,
  },

  buttonBack: {
    backgroundColor: 'red',
    width: '45%',
    padding: 10,
    alignItems: 'center',
    borderRadius: 10,
  },

  buttonScan: {
    backgroundColor: 'green',
    width: '45%',
    padding: 10,
    alignItems: 'center',
    borderRadius: 10,
  },

  buttonInative: {
    backgroundColor: 'gray',
    width: '45%',
    padding: 10,
    alignItems: 'center',
    borderRadius: 10,
    opacity: 0.5,
  },

  textButton: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 12,
  },

  scan_area: {
    width: 250,
    borderWidth: 2,
    borderColor: 'red',
    height: 250,
    position: 'absolute',
    bottom: (Dimensions.get('window').height - 90) / 2,
  },
});

export default styles;