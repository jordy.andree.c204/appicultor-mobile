import * as React from "react";
import styles from "./Camera_Styles";
import {
  Text,
  View,
  TouchableOpacity,
  Alert,
  StyleSheet,
  Dimensions,
} from "react-native";
import { BarCodeScanner } from "expo-barcode-scanner";

//DEFININDO O ESPAÇO DO SCANEAMENTO DA CÂMERA
const finderWidth = 250;
const finderHeight = 250;
const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
const viewMinX = (width - finderWidth) / 2;
const viewMinY = (height - finderHeight) / 2;

export default function Camera(props) {
  const [hasPermission, setHasPermisson] = React.useState(null);
  const [scanned, setScanned] = React.useState(false);

  //PEGANDO OS VALORES E FUNCÕES RECEBIDOS VIA PARAMETROS
  const setScaneado = props.route.params.setScaneado;
  const setAgain = props.route.params.setAgain;
  const setHandle = props.route.params.handleeditItem;
  const params = props.route.params.params;

  React.useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermisson(status === "granted");
    })();
  }, []);

  const handleBarCodeScanned = ({ type, data, bounds }) => {
    //PEGANDO A POSIÇÃO DO ITEM SCANEADO
    const { x, y } = bounds.origin;
    //VERIFICANDO SE O ITEM SCANEADO ESTÁ DENTRO DA AREA PERMITIDA
    if (
      x >= viewMinX &&
      y >= viewMinY &&
      x <= viewMinX + finderWidth / 2 &&
      y <= viewMinY + finderHeight / 2
    ) {
      setScanned(true);
      var cod_scaneado = data;
      //ATUALIZANDO A LISTA DE ITENS SCANEADOS
      setScaneado(cod_scaneado);
      setAgain(1)
      props.navigation.navigate("Caderno", params);
    }
  };

  return (
    <View style={styles.container}>
      <BarCodeScanner
        barCodeTypes={[BarCodeScanner.Constants.BarCodeType.code39]}
        onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
      />
      <View style={styles.header}>
        <Text style={styles.headerText}>
          APONTE PARA LER O CODIGO DE BARRA!
        </Text>
      </View>
      <View style={styles.scan_area}></View>
      <View style={styles.containerButtons}>
        <TouchableOpacity
          style={styles.buttonBack}
          onPress={() => props.navigation.navigate("Caderno", params)}
        >
          <Text style={styles.textButton}>VOLTAR</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
